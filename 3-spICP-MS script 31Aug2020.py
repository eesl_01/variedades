#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Aug  3 08:07:32 2019

@author: saurav/muammar
"""

import numpy as np
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
from os import walk
import os

""" EESL questions
no need for time information at all?
"""

# function to find iterative mean
def meanFinder (timeseries, lastMean,stdMultiplier):
    # This will iteratively find mean
    # LastMean on the first iteration should be 0. This is updated by the code and called recursively
    # Timeseries is the pandas series of interest
    # stdMultplier defines what should be considered outlier. Cutoff is computed as
    # mean + stdMultiplier * stDeviation. The default is 3.
    newMean = timeseries.mean().iloc[0]
    stDev = timeseries.std()
    cutoff = newMean + stdMultiplier * stDev
    cutoff = cutoff.iloc[0]
    #print (newMean)
    if newMean == lastMean:
        #horray we are done
        return(newMean,cutoff)
    else:
        new_timeseries = timeseries[timeseries<=cutoff]
        #print ("Mean:{}, Cutoff:{}, Items:{}".format(newMean,cutoff,new_timeseries.count().iloc[0]))
        return meanFinder(new_timeseries, newMean, stdMultiplier)

def prepend_multiple_lines(file_name, list_of_lines):
    """Insert given list of strings as a new lines at the beginning of a file"""
    # define name of temporary dummy file
    dummy_file = file_name + '.bak'
    # open given original file in read mode and dummy file in write mode
    with open(file_name, 'r') as read_obj, open(dummy_file, 'w') as write_obj:
        # Iterate over the given list of strings and write them to dummy file as lines
        for line in list_of_lines:
            write_obj.write(line + '\n')
        # Read lines from original file one by one and append them to the dummy file
        for line in read_obj:
            write_obj.write(line)
    # remove original file
    os.remove(file_name)
    # Rename dummy file as the original file
    os.rename(dummy_file, file_name)

""" User input section """
# Variables to customize
folderPath = "data_muammar" # where are the files. This can be absolute or relative
#Instrument Parameters
dwellTime = 0.1   #unit milliseconds
scanTime =  40    #unit seconds
flowRate =  0.466 #unit mL/min
TE = 0.037      #Calculate from Au stds. Use the "Worksheet TE calculator.xlsx"
scanTimeMs= scanTime*1000
flowRate_ml_ms = flowRate/60/1000
#Algorithm Quality Check
stdMultiplier = 3  #sigma value to differentiate pulse points over background noise
minimumCutoff = 2 # the iterative mean (stableMean) should be more than this otherwise use minimumCutoff
minimumConsecutiveOutliers = 0 # anything less than this number+1 does not constitute a pulse. 2 is default. Don't go over 4.
cumulativeOutlier = 98  #in %. Take only cumulative counts that reach 98%. Other 2% considered as outliers
#Calculation Constants
slopeDissolved = 2.3789      #concentration on x-axis. Element-specific  
interceptDissolved = 95.5806   #intensity on y-axis. Element-specific
slopeMass = 8.255e10
particleDensity = 3.8  #unit g/cm3
massFraction = 0.523
pd_ug_nm3=particleDensity*1e-15
#Plotting Constants
binSizeMass = 2  #0.2 fg is good for Au. For other elements, use 2 fg
binSizeDia = 2   #use 2 or more. 1 nm bins tend to result in holes in the middle of the graph
minBinSizeDia = 0  #approximate detection limit. Element- and particle-specific. Calculate from Lee et al. (2014)

""" End user input section """

# Lets find CSV files of interest 
files = []
for (dirpath, dirnames, filenames) in walk(folderPath):
    files = [os.path.splitext(i)[0] for i in filenames if "csv" in i]   #eesl
#print (files)

output= []
for_ratios = np.array([])

# Processing one file at a time (may be parallelized if needed)
for fileName in files:
    store_dir = os.path.join(os.path.abspath(os.path.curdir),folderPath + '_analysis', 'analysis_%s' % fileName)
    if not os.path.isdir(store_dir):
        os.makedirs(store_dir)

    print ("processing file {}.csv ".format(fileName))
    # df = pd.read_csv(folderPath+fileName+".csv", skiprows = 3, usecols = range(1,2))

    # eesl
    df_all = pd.read_csv(os.path.join(folderPath, fileName + ".csv"), skiprows=3)
    df_all = df_all[~df_all['Time [Sec]'].str.contains("Printed", na=False)]

    n_elements = len(df_all.columns) - 1
    counter = 0
    print('working with < %s > different elements...' % n_elements)

    for cur_col in range(1, n_elements+1):
        df = df_all.iloc[1:,cur_col].to_frame() # to prevent ghost columns with improper CSV ... only first column read
        get_times = df_all.iloc[1:,0].to_frame()
        element_name = '_' + list(df.columns)[0].strip("''")
        print('element < %s >:\n' % element_name)
        print(df.info())

        (newMean,cutoff) = meanFinder(df,0,stdMultiplier)
        print ("mean: {} , cutoff:{}".format(newMean,cutoff) )

        if cutoff < minimumCutoff:
            print("Computed cutoff {} less than noise level {}.Using default cutoff values.".format(cutoff,minimumCutoff))
            cutoff = minimumCutoff

        #calculate detection limits
        cutoffMass = (1/massFraction*(cutoff-newMean)/slopeMass)*(minimumConsecutiveOutliers+1)
        cutoffDiameter =(6*cutoffMass/ np.pi / pd_ug_nm3)**(1/3)

        t_range_outliers = get_times[df>cutoff]
        outliers = df[df>cutoff]
        #Shifting outliers to create a data frame.
        shiftingBy = np.arange(-1,2) # this will create an array [-1,0,1]
        a = [ outliers.shift(i) for i in shiftingBy]
        dfShifted = pd.concat(a,axis=1) # in this data frame if each row is Nan then the data is not outlier... one nan is edge... both numbers is outlier
        dfO=df[dfShifted.sum(axis = 1, skipna = True)>0] # gets sum neglecting NAN; two NANS will be zero... note this is used to get df (original df)
        # if df0 is less than cutoff then these are edge values, not part of the required set
        dfOutliers = dfO[dfO>cutoff]
        dfOutliers = dfOutliers.dropna(axis=0)
        #New addition below to minus the baseline
        dfOutliers["E"] =  dfOutliers.iloc[:,0] - newMean
        #some steps to assessing a group
        dfOutliers["index"]= dfOutliers.index
        dfOutliers["indexDiff"] = dfOutliers["index"]-dfOutliers["index"].shift()
        dfOutliers["group"] = dfOutliers["index"]
        dfOutliers.loc[dfOutliers["indexDiff"] <2,"group"] = np.NaN
        dfOutliers["group"]= dfOutliers["group"].fillna(method="ffill")
        #print(dfOutliers)

        #Collect outlier points above the sigma threshold
        dfO_group = dfOutliers.groupby("group")
        dfO_= dfO_group.filter(lambda x: len(x) > minimumConsecutiveOutliers)
        dfO_group_new = dfO_.groupby("group")#removing false positives

        # Get the counts per group:
        counts_pgroup = dfO_.groupby("group").agg(['min', 'max','count'])['index']['count']
        mint_pgroup = dfO_.groupby("group").agg(['min', 'max','count'])['index']['min']
        maxt_pgroup = dfO_.groupby("group").agg(['min', 'max','count'])['index']['max']

        # Group and sum each pulse
        # groupedIntensities = dfO_group_new.sum().iloc[:,0]
        groupedIntensities = dfO_group_new.sum().iloc[:, 1]
        print ("Sums Follow:")

        #calculate particleMass and histograms to remove outliers if there are sufficient particles
        particleMass = 1/massFraction*(groupedIntensities)/slopeMass
        # saving histogram data for Mass
        #binSize defined below so as to be dynamic with respect to range
        if len(groupedIntensities)>50:
            binSize = (particleMass.mean() - min(particleMass))/20
            bins=np.arange(min(particleMass),max(particleMass)+binSize, binSize)
            histMass = np.histogram(particleMass,bins)
            _dfMass = pd.DataFrame(histMass).T
            _dfMass.columns=["count","bin_edges"]
            _dfMass['CUMSUM'] = _dfMass['count'].cumsum()
            cumMax = _dfMass['CUMSUM'].max()
            _dfMass['relativeCUMSUM'] = _dfMass['CUMSUM']/cumMax * 100
            _dfMass = _dfMass[_dfMass.relativeCUMSUM < cumulativeOutlier]
            _dfMass.to_csv(os.path.join(store_dir, fileName+element_name+"_mass_hist.csv"))
            #_dfMass
            #filter pulses to remove those with intensities higher than the cutoff
            massMax = _dfMass['bin_edges'].max()
            intensityCutoff = massMax*slopeMass*massFraction
            groupedIntensities2 = groupedIntensities[groupedIntensities < intensityCutoff]
            #todo: Perhaps here also filter mystats_pgroup ???
        else:
            groupedIntensities2 = groupedIntensities
        #DATA NOW QUALITY FILTERED VIA 98% COUNTS IF THERE ARE OVER 50 PARTICLES DETECTED

        # Extract the times corresponding to teh grouped intsnsities.
        # The reatios are from grouped intensities (look at th epower point presentation)
        # Get the number of points forming a pulse
        mystats = pd.DataFrame(
            np.c_[counts_pgroup.to_numpy(), mint_pgroup.to_numpy(), maxt_pgroup.to_numpy(), groupedIntensities2.to_numpy()],
            columns=["%s_%s" %(counts_pgroup.name, cur_col), mint_pgroup.name, maxt_pgroup.name, 'intens.%s'%cur_col])

        if n_elements > 1:
            if counter == 0:
                mystats_cur = mystats
            elif counter > 0:
                mystats_cur = pd.concat([mystats_cur, mystats], ignore_index=True)
            if counter == (n_elements-1):
                mystats_all = mystats_cur
                mystats_all = mystats_all.sort_values(mint_pgroup.name,ascending=True)
                mystats_all.reset_index(inplace=True, drop=True)
                # Calculate rates: works only for first/second element
                ratios = pd.DataFrame(mystats_all['intens.1']/mystats_all['intens.2'], columns=['ratios'])
                mystats_all = mystats_all.join(ratios)
            counter += 1

        elif n_elements == 1:
            mystats_all = pd.DataFrame(
                np.c_[counts_pgroup.to_numpy(), mint_pgroup.to_numpy(), maxt_pgroup.to_numpy(), groupedIntensities2.to_numpy()],
                columns=[counts_pgroup.name, mint_pgroup.name, maxt_pgroup.name, 'intens.'])

        # some computations
        numGrp = len(groupedIntensities2)
        conc = (newMean - interceptDissolved)/slopeDissolved
        if conc < 0:
            conc = 0
        particleConc = numGrp/scanTimeMs/flowRate_ml_ms/TE
        particleMass2 = 1/massFraction*(groupedIntensities2)/slopeMass
        particleMass2_fg = particleMass2*1e9
        particleSum_ug_L = sum(particleMass2)/scanTimeMs/flowRate_ml_ms/TE*1000
        totalSum_ug_L = conc + particleSum_ug_L
        if totalSum_ug_L != 0:
            fparticle_total = particleSum_ug_L/totalSum_ug_L*100
        else:
            fparticle_total = 0
        particleDiameter =(6*particleMass2/ np.pi / pd_ug_nm3)**(1/3)


        #Output CSV files for each sample
        dfOutliers.to_csv(os.path.join(store_dir,fileName+element_name+"_OutlierPoints.csv"))
        groupedIntensities2.to_csv(os.path.join(store_dir,fileName+element_name+"_GroupedPulseIntensities.csv"))
        particleMass2_fg.to_csv(os.path.join(store_dir,fileName+element_name+"_pMass.csv"))
        particleDiameter.to_csv(os.path.join(store_dir,fileName+element_name+"_pDia.csv"))
        # creating a single output file
        output.append([fileName+element_name,newMean,cutoff,
                       cutoffMass,
                       conc,totalSum_ug_L,
                       particleSum_ug_L,fparticle_total,
                       numGrp,particleConc,
                       groupedIntensities2.mean(),groupedIntensities2.median(),groupedIntensities2.std(),
                       particleMass2_fg.mean(),particleMass2_fg.median(),particleMass2_fg.std(),
                       particleMass2_fg.min(), particleMass2_fg.max(),
                       particleMass2_fg.skew(),particleMass2_fg.kurtosis(),
                       cutoffDiameter,
                       particleDiameter.mean(),particleDiameter.median(),particleDiameter.std(),
                       particleDiameter.min(), particleDiameter.max(),
                       particleDiameter.skew(),particleDiameter.kurtosis()])
        _d = pd.DataFrame(output,columns=["fileName","Dissolved mean (intensity)","3sigma cutoff (intensity)",
                                          "DL mass (ug)",
                                          "Dissolved concentration (ug/L)","Total measurable concentration (ug/L)",
                                          "particleConc (ug/L)", "% particle/ total mass",
                                          "# of pulses","particleConc (particles/ml)",
                                          "Mean pulse (intensity)","Median pulse (intensity)","Stdev pulse (intensity)",
                                          "Mean pulse (fg)","Median pulse (fg)","Stdev pulse (fg)",
                                          "Min pulse (fg)", "Max pulse (fg)",
                                          "Mass skewness","Mass kurtosis",
                                          "DL size (nm)",
                                          "Mean size (nm)","Median size (nm)","Stdev size (nm)",
                                          "Min size (nm)", "Max size (nm)",
                                          "Size skewness","Size kurtosis"])
        _d.to_csv(os.path.join(store_dir,fileName+"_Output.csv"))

        # generating figures and saving files
        #Define 4 figures in one plot space
        f, axes = plt.subplots(2, 2, figsize=(7, 7))
        #Parameters for figure 1 - raw intensity histogram
        axes[0,0].set_title("Intensities, automatic bin size")
        sns.distplot(df.iloc[:,0], ax = axes[0,0], kde=False, color='green')
        axes[0,0].set(xlabel="Raw intensity", ylabel="Frequency")
        axes[0,0].set_yscale('log')
        #sns.distplot(np.log10(df['E']), ax = axes[0,0]) ##for log scale

        #Generate the next 3 figures if there are particles
        if len(particleDiameter)>1:
            #Parameters for figure 2 - mass histogram in fg
            binSize = (particleMass2_fg.mean() - min(particleMass2_fg))/20
            if binSize <= 1:
                binSize = 1
            bins=np.arange(min(particleMass2_fg),max(particleMass2_fg)+binSize, binSize)
            axes[0,1].hist(particleMass2_fg,bins,width=binSize,color='red',edgecolor='black',linewidth=0.3)
            #sns.distplot(particleMass2,bins=bins,ax = axes[0,1],kde=False),
            axes[0,1].set_title("Mass, automatic bin size")
            axes[0,1].set(xlabel="Particle Mass (fg)", ylabel="Frequency")
            #Parameters for figure 3 - size histogram with specified bin size
            #bins=np.arange(min(particleDiameter),max(particleDiameter)+binSizeDia,binSizeDia)
            bins=np.arange(minBinSizeDia,max(particleDiameter)+binSizeDia,binSizeDia)
            #saving histogram data for diameter
            histDia = np.histogram(particleDiameter,bins)
            _dfDia = pd.DataFrame(histDia).T
            _dfDia.columns=["count","bin_edges"]
            _dfDia.to_csv(os.path.join(store_dir,fileName+element_name+"_dia_hist.csv"))
            axes[1,0].hist(particleDiameter,bins,width=binSizeDia,color='yellow',edgecolor='black',linewidth=0.3)
            axes[1,0].set_title("Diameter, bin size {}".format(binSizeDia))
            axes[1,0].set(xlabel="Size (nm)", ylabel="Frequency")
            #sns.distplot(particleDiameter,bins=bins ,ax=axes[1,0],kde=False)

            #Parameters for figure 4 - size histogram with automatic bin size
            axes[1,1].set_title("Diameter, automatic bin size")
            sns.distplot(particleDiameter,ax=axes[1,1],kde=False)
            axes[1,1].set(xlabel="Size (nm)", ylabel="Frequency")

        #Finally, do final formatting and save the figure
        f.tight_layout(rect=[0, 0.03, 1, 0.95])
        f.suptitle(fileName)
        plt.savefig(os.path.join(store_dir,fileName+element_name+".png"),dpi=300)
    

    list_of_lines = ["Instrument Parameters",
                     "Dwell time = {} ms".format(dwellTime),
                     "Scan time = {} s".format(scanTime),
                     "Flow rate = {} mL/min".format(flowRate),
                     "Transport efficiency = {}".format(TE),
                     "Algorithm Quality Check",
                     "Background threshold = {} sigma".format(stdMultiplier),
                     "Minimum points for a pulse = {} datapoints".format(minimumConsecutiveOutliers+1),
                     "Cumulative cutoff = {}%".format(cumulativeOutlier),
                     "Calculation Constants",
                     "Slope dissolved = {}".format(slopeDissolved),
                     "Intercept dissolved = {}".format(interceptDissolved),
                     "Slope mass = {}".format(slopeMass),
                     "Particle density = {} g/cm3".format(particleDensity),
                     "Mass fraction = {}".format(massFraction),
                     "\n"
                     ]
    header_ratios=['index, numpar1, tini, tend, numpar2, intens1, intens2, meanIntensityratios']
    mystats_all.to_csv(os.path.join(store_dir, fileName + "_ratios.csv"), na_rep='nan')
    prepend_multiple_lines(os.path.join(store_dir, fileName + "_ratios.csv"), header_ratios)
    prepend_multiple_lines(os.path.join(store_dir,fileName+"_Output.csv"), list_of_lines)
