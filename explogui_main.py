#!/home/popo/anaconda3/bin/python
# -*- coding: utf-8 -*-
"""
    GUI para determinar la magnitud de una explosión registrada
     en la estacion PPIG a partir de las relaciones encontradas
     por Cruz-Atienza et al. (2001)
     Desarrollado por:
     Oscar Castro Artola, Instituto de Geofisica, diciembre 2018
"""
from PyQt5.QtGui import *
from PyQt5 import QtCore
from PyQt5.QtWidgets import QDialog
from PyQt5.QtWidgets import QApplication
from PyQt5.QtCore import QTime
from PyQt5.QtWidgets import QGridLayout,QLabel,QGroupBox
from PyQt5.QtWidgets import QVBoxLayout,QLineEdit,QCheckBox,QHBoxLayout
from PyQt5.QtWidgets import QComboBox,QCalendarWidget,QPushButton
from PyQt5.QtWidgets import QButtonGroup
from PyQt5.QtWidgets import QFileDialog
from explogui_func import get_data_here
from explogui_func import plot_all
from explogui_func import save_txt
from explogui_func import calc_mag
from explogui_func import mag_stats
import matplotlib.pyplot as plt
from matplotlib.backends.backend_qt4agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.backends.backend_qt4agg import NavigationToolbar2QT as NavigationToolbar
import warnings
import os
import subprocess as sp
warnings.filterwarnings("ignore")


class MainWindow(QDialog):
    # MAIN FUNCTION
    def __init__(self):
        super(MainWindow, self).__init__()
        self.setWindowTitle(self.tr("EXPLOSION MAGNITUDE CALCULATOR"))
#        self.resize(1500,100)
        # LAYOUT
        self.grid = QGridLayout()
        self.setLayout(self.grid)
        # WIDGETS
        self.image = QLabel(self)
#        self.image.setGeometry(10, 20, 1000, 190)
        self.pixmap = QPixmap("/home/popo/popo_explo/main/img/logo_CENAPRED.jpeg")
        width1, height1 = 300,111
        self.image.setPixmap(self.pixmap.scaled(width1, height1))

        self.imageunam = QLabel(self)
        self.pixmapunam = QPixmap("/home/popo/popo_explo/main/img/logo_unam.png")
        width2, height2 = 100,118
        self.imageunam.setPixmap(self.pixmapunam.scaled(width2, height2))
#        self.image.setMaximumWidth(300)
        self.image.show()
        self.imageunam.show()

        # Group 1
        self.group_1 = QGroupBox('-- Seleccionar fecha y estación --',self)
        self.vbox_1 = QVBoxLayout()
        # Group 1_1
        self.group_1_1 = QGroupBox('-- Ingresar fecha --',self)
        self.vbox_1_1 = QVBoxLayout()
        self.line_date = QLineEdit()
        self.chkbxdatein = QCheckBox("Usar",self)
        self.line_date.setFixedWidth(180)
        self.label_fmt = QLabel("Formato:\nYYYY/MM/DD, HH:MM:SS", self)
        self.vbox_1_1.addWidget(self.label_fmt)
        self.vbox_1_1.addWidget(self.line_date)
        self.vbox_1_1.addWidget(self.chkbxdatein)
        self.group_1_1.setLayout(self.vbox_1_1)
        self.group_1_1.setMaximumWidth(250)

        # Group 1_2
        self.group_1_2 = QGroupBox('-- Seleccionar fecha --',self)
        self.vbox_1_2 = QVBoxLayout()

        self.group_1_2_1 = QGroupBox("-- Hora --",self)
        self.hbox_1_2_1 = QHBoxLayout()
        self.combo_sta = QComboBox()
        self.combo_hour = QComboBox()
        self.combo_min = QComboBox()
        self.combo_sec = QComboBox()
        for item in ['PPC  - Colibrí','PPP  - Canario','PPX  - Chipiquixtle','PPIG - Tlamacas (SSN)']:
            self.combo_sta.addItem(item)
        hour_list = range(0,24)
        for item in hour_list:
            self.combo_hour.addItem("%02d" % (int(str(item))))
        min_list = range(0,60)
        for item in min_list:
            self.combo_min.addItem("%02d" % (int(str(item))))
        for item in min_list:
            self.combo_sec.addItem("%02d" % (int(str(item))))
        self.labelcomp = QLabel("Seleccionar estación:", self)
        self.labelhour = QLabel("HH", self)
        self.labelmin = QLabel("MM", self)
        self.labelsec = QLabel("SS", self)
        self.calendar = QCalendarWidget()
        self.hbox_1_2_1.addWidget(self.labelhour)
        self.hbox_1_2_1.addWidget(self.combo_hour)
        self.hbox_1_2_1.addWidget(self.labelmin)
        self.hbox_1_2_1.addWidget(self.combo_min)
        self.hbox_1_2_1.addWidget(self.labelsec)
        self.hbox_1_2_1.addWidget(self.combo_sec)
        self.group_1_2_1.setLayout(self.hbox_1_2_1)
        self.combo_hour.setFixedWidth(50)
        self.combo_min.setFixedWidth(50)
        self.combo_sec.setFixedWidth(50)
        self.group_1_2_1.setMaximumWidth(350)
        self.combo_sta.setFixedWidth(110)
        self.chkbxdatesel = QCheckBox("Usar",self)
        self.vbox_1_2.addWidget(self.group_1_2_1)
        self.vbox_1_2.addWidget(self.calendar)
        self.vbox_1_2.addWidget(self.chkbxdatesel)
        self.group_1_2.setLayout(self.vbox_1_2)
        self.group_1_2.setMaximumWidth(350)

        self.bot_get_info = QPushButton("Buscar datos")
        self.bot_get_info.setDisabled(True)
        self.bot_get_info.setFixedWidth(180)
        self.vbox_1.addWidget(self.group_1_1)
        self.vbox_1.addWidget(self.group_1_2)
        self.vbox_1.addWidget(self.labelcomp)
        self.vbox_1.addWidget(self.combo_sta)
        self.vbox_1.addWidget(self.bot_get_info)
        self.labelnotfile = QLabel()
        self.vbox_1.addWidget(self.labelnotfile)
        self.group_1.setLayout(self.vbox_1)
        self.group_1.setMaximumWidth(350)
        # 
        self.bot_get_mag = QPushButton("INICIAR CÁLCULO")
        self.bot_get_mag.setDisabled(True)
        # Group 2
        self.group_2 = QGroupBox('-- Magnitudes calculadas por estación --',self)
        self.group_2.setMaximumWidth(300)
        self.vbox_2 = QVBoxLayout()
        self.info1_mag_lab = QLabel()
        self.info2_mag_lab = QLabel()
        self.info1_mag_lab.setStyleSheet('color: red')
        self.info2_mag_lab.setStyleSheet('color: red')
        self.info1_mag_lab.setText("      STA  |    MAG    |   FILTRO [Hz]")
        self.info2_mag_lab.setText("------------------------------------------------------------")
        self.ppc_mag_lab = QLabel()
        self.ppp_mag_lab = QLabel()
        self.ppx_mag_lab = QLabel()
        self.ppi_mag_lab = QLabel()
        self.vbox_2.addWidget(self.info1_mag_lab)
        self.vbox_2.addWidget(self.info2_mag_lab)
        self.vbox_2.addWidget(self.ppc_mag_lab)
        self.vbox_2.addWidget(self.ppp_mag_lab)
        self.vbox_2.addWidget(self.ppx_mag_lab)
        self.vbox_2.addWidget(self.ppi_mag_lab)
        self.bot_new_mag = QPushButton("NUEVO CÁLCULO")
        self.bot_new_mag.setDisabled(True)
        self.group_2.setLayout(self.vbox_2)
        # Group 3
        self.group_3 = QGroupBox('-- Procesado --',self)
        self.vbox_1 = QVBoxLayout()
        self.image_volc = QLabel(self)
        self.pixmap_volc = QPixmap()
        width, height = 250,170
        self.image_volc.setPixmap(self.pixmap_volc.scaled(width, height))
        self.line_freq1 = QLineEdit()
        self.line_freq1.setFixedWidth(80)
        self.labelfiltro = QLabel("<h3>Filtrar datos</h3>", self)
        self.labelt1 = QLabel("Frecuencia inicial (0.025 - 0.05) [Hz]", self)
        self.line_freq2 = QLineEdit()
        self.line_freq2.setFixedWidth(80)
        self.labelt2 = QLabel("Frecuencia final (0.1 - 0.5) [Hz]", self)
        self.bot_filtrar = QPushButton("Filtrar datos")
        self.bot_filtrar.setDisabled(True)
#        self.bot_filtrar.setFixedWidth(180)
        self.vbox_1.addWidget(self.image_volc)
        self.vbox_1.addWidget(self.labelfiltro)
        self.vbox_1.addWidget(self.labelt1)
        self.vbox_1.addWidget(self.line_freq1)
        self.vbox_1.addWidget(self.labelt2)
        self.vbox_1.addWidget(self.line_freq2)
        self.vbox_1.addWidget(self.bot_filtrar)
        self.group_3.setLayout(self.vbox_1)
        self.group_3.setMaximumWidth(250)
        self.image_volc.show()
        # Group 4
        self.group_4 = QGroupBox('-- Salvar resultados --',self)
        self.vbox_1 = QVBoxLayout()
        self.bot_salvartxt = QPushButton("Guardar archivo")
        self.bot_salvartxt.setDisabled(True)
        self.bot_salvartxt.setFixedWidth(180)
        self.vbox_1.addWidget(self.bot_salvartxt)
        self.labeltxtsave = QLabel()
        self.vbox_1.addWidget(self.labeltxtsave)
        self.bot_salvartxt.setFixedWidth(180)
        self.group_4.setLayout(self.vbox_1)
        self.group_4.setMaximumWidth(250)
        # Group 5
        self.group_5 = QGroupBox('-- Magnitud calculada --',self)
        self.vbox_1 = QVBoxLayout()
        self.labelmag = QLabel("<h1> M <sub>k</sub>  =   </h1>", self)
        self.vbox_1.addWidget(self.labelmag)
        self.group_5.setLayout(self.vbox_1)
        self.group_5.setMaximumWidth(250)
        # Non-groups
        self.figure,ax = plt.subplots(figsize=(20, 6), facecolor='w', edgecolor='k')
        self.canvas = FigureCanvas(self.figure)
        self.toolbar = NavigationToolbar(self.canvas, self)
        self.bot_exit = QPushButton("Salir")
        # LABEL 
        self.label = QLabel("<h1> Calculador de explosiones para el Popocatepetl</h1>", self)
        self.labelauthor = QLabel("Elaborado y desarrollado por: Dr. Oscar A. Castro Artola\nInstituto de Geofísica, UNAM", self)
        # ADD WIDGETS
        self.grid.addWidget(self.imageunam,1,1)
        self.grid.addWidget(self.image,1,2)
        self.grid.addWidget(self.label,1,3)
        self.grid.addWidget(self.group_1,2,1)
        self.grid.addWidget(self.bot_get_mag,3,1)
        self.grid.addWidget(self.group_2,4,1)
        self.grid.addWidget(self.bot_new_mag,5,1)
        self.grid.addWidget(self.group_3,2,5)
        self.grid.addWidget(self.group_4,3,5)
        self.grid.addWidget(self.group_5,4,5)
        self.grid.addWidget(self.toolbar,6,2,6,3)
        self.grid.addWidget(self.labelauthor,5,2)
        self.grid.addWidget(self.canvas,2,2,3,3)
        self.grid.addWidget(self.bot_exit,5,5)
#        # CONECTIONS
        self.line_date.textChanged.connect(lambda: self.line_date.text())
        self.bot_get_info.clicked.connect(self.get_info)
        self.bot_get_info.clicked.connect(self.enable_save_text)
        self.bot_get_info.clicked.connect(self.enable_filtrar)
        self.bot_get_info.clicked.connect(self.update_img)
        self.bot_get_info.clicked.connect(self.enable_calc_mag)
        self.bot_get_info.clicked.connect(self.enable_new_mag)
        self.bot_get_mag.clicked.connect(self.get_mag)
        self.bot_new_mag.clicked.connect(self.new_mag)
        self.bot_filtrar.clicked.connect(self.filtrar_func)
        self.bot_salvartxt.clicked.connect(self.salvar_texto)
        self.bot_exit.clicked.connect(self.close)

        self.chkbxdatein.stateChanged.connect(self.toggle_chkbx_info)
        self.chkbxdatesel.stateChanged.connect(self.toggle_chkbx_info)
        
        self.bg  =QButtonGroup()
        self.bg.addButton(self.chkbxdatein,1)
        self.bg.addButton(self.chkbxdatesel,2)

        if self.chkbxdatein.isChecked():
            self.bot_get_info.setEnable()
        global magnitudes
        magnitudes = {}

    # FUNCTIONS
    def toggle_chkbx_info(self):
        self.bot_get_info.setDisabled(False)

    def get_info(self):
        import os.path
        from sys import exit
        global datfile,mag,t0,sta,y,mo,d,h,mi,s
        sta=str(self.combo_sta.currentText()).split("-")[0].replace(" ", "")
        datfile=[]
        if self.chkbxdatein.isChecked():
            date = str(self.line_date.text())
            if date:
                comp = "HHZ"
                y  = date.split(",")[0].split("/")[0]
                mo = date.split(",")[0].split("/")[1]
                d  = date.split(",")[0].split("/")[2]
                h  = "%02d" % int(date.split(",")[1].split(":")[0])
                mi = date.split(",")[1].split(":")[1]
                s  = date.split(",")[1].split(":")[2]
                self.chk_date(y,mo,d,h,mi,s)
                datfile = get_data_here(y,mo,d,h,mi,s,sta)
                cont=0
                for ifile in datfile:
                    if os.path.isfile(ifile):
                        cont=cont+1
                    else: 
                        self.labelnotfile.setStyleSheet('color: red')
                        self.labelnotfile.setText("Algún archivo SAC no existe")
            else: 
                self.labelnotfile.setStyleSheet('color: red')
                self.labelnotfile.setText("La cadena esta vacia")
        elif self.chkbxdatesel.isChecked():
            comp = "HHZ"
            date = self.calendar.selectedDate().toPyDate()
            y = str(date).split("-")[0]
            mo = str(date).split("-")[1]
            d = str(date).split("-")[2]
            h = str(self.combo_hour.currentText())
            mi = str(self.combo_min.currentText())
            s = str(self.combo_sec.currentText())
            self.chk_date(y,mo,d,h,mi,s)
            datfile = get_data_here(y,mo,d,h,mi,s,sta)
            cont=0
            for ifile in datfile:
                if os.path.isfile(ifile):
                    cont=cont+1
                else: 
                    self.labelnotfile.setStyleSheet('color: red')
                    self.labelnotfile.setText("Algún archivo SAC no existe")
        else:
            print("Selecciona una fecha")
            self.labelnotfile.setStyleSheet('color: red')
            self.labelnotfile.setText("Selecciona una casilla")
        if cont == 3:
            self.canvas.draw()
            self.labelnotfile.setStyleSheet('color: green')
            self.labelnotfile.setText("Todos los archivos SAC existen")
            plot_all(datfile,sta,y,mo,d,h,mi,s)
            self.canvas.draw()

    def filtrar_func(self):
        global frq1,frq2
        if self.line_freq1.text() and self.line_freq2.text():
            frq1 = float(self.line_freq1.text())
            frq2 = float(self.line_freq2.text())
        else:
            frq1 = 0.0333
            frq2 = 0.1
        plot_all(datfile,sta,y,mo,d,h,mi,s,f1=frq1,f2=frq2)
        self.canvas.draw()

    def new_mag(self):
        global magnitudes, mag_final, std_dev
        magnitudes = {}
        mag_final = []
        std_dev = []
        self.ppc_mag_lab.setText("")
        self.ppp_mag_lab.setText("")
        self.ppx_mag_lab.setText("")
        self.ppi_mag_lab.setText("")
        self.labelmag.setText("")

    def get_mag(self):
        global magnitudes,t0,frq1,frq2,mag_final,std_dev
        if self.line_freq1.text() and self.line_freq2.text():
            frq1 = float(self.line_freq1.text())
            frq2 = float(self.line_freq2.text())
        else:
            frq1 = 0.0333
            frq2 = 0.1
        (mag, t0) = calc_mag(datfile,sta,y,mo,d,h,mi,s,f1=frq1,f2=frq2)
        if sta == "PPC":
            self.ppc_mag_lab.setStyleSheet('color: navy')
            self.ppc_mag_lab.setText("     %s   |    %3.2f   |  %5.3f - %5.3f" % (sta, mag, frq1,frq2))
            magnitudes[sta] = mag
        elif sta == "PPP":
            self.canvas.draw()
            self.ppp_mag_lab.setStyleSheet('color: olive')
            self.ppp_mag_lab.setText("     %s   |    %3.2f   |  %5.3f - %5.3f" % (sta, mag, frq1,frq2))
            magnitudes[sta] = mag
        elif sta == "PPX":
            self.canvas.draw()
            self.ppx_mag_lab.setStyleSheet('color:blue ')
            self.ppx_mag_lab.setText("     %s   |    %3.2f   |  %5.3f - %5.3f" % (sta, mag, frq1,frq2))
            magnitudes[sta] = mag
        elif sta == "PPIG":
            self.canvas.draw()
            self.ppi_mag_lab.setStyleSheet('color: teal')
            self.ppi_mag_lab.setText("     %s  |   %3.2f    |  %5.3f - %5.3f" % (sta, mag, frq1,frq2))
            magnitudes[sta] = mag
        (mag_final, std_dev) = mag_stats(magnitudes)
        self.canvas.draw()
        self.labelmag.setStyleSheet('color: green')
        self.labelmag.setText("<h2> M <sub>k</sub> =  %4.2f +/- %5.3f </h2> " % (mag_final,std_dev))

    def salvar_texto(self):
        ofilename = save_txt(magnitudes,t0,frq1,frq2,mag_final,std_dev)
        self.labeltxtsave.setStyleSheet('color: red')
        self.labeltxtsave.setText("Archivo guardado:\n%s" % ofilename)
    def enable_save_text(self):
        self.bot_salvartxt.setDisabled(False)
    def enable_filtrar(self):
        self.bot_filtrar.setDisabled(False)
    def enable_buscar_dat(self):
        self.bot_get_info.setDisabled(False)
    def enable_calc_mag(self):
        self.bot_get_mag.setDisabled(False)
    def enable_new_mag(self):
        self.bot_new_mag.setDisabled(False)
    def chk_date(self,y,mo,d,h,mi,s):
        if not y or int(y) <= 1979 or int(y) >= 2100:
            print("[WARNING] Year format is wrong")
            exit()
        if not mo or int(mo) < 1 or int(mo) >= 12:
            print("[WARNING] Month format is wrong")
            exit()
        else:
            mo = str("%02d" % int(mo))
        if  not d or int(d) < 1 or int(d) >= 31:
            print("[WARNING] Day format is wrong")
            exit()
        else:
            d = str("%02d" % int(d))
        if not h or int(h) < 0 or int(h) > 23:
            print("[WARNING] Hour format is wrong")
            exit()
        else:
            h = str("%02d" % int(h))
        if not mi or int(mi) < 0 or int(mi) > 59.99:
            print("[WARNING] Minute format is wrong")
            exit()
        else:
            mi = str("%02d" % int(mi))
        if not s or int(s) < 0 or int(s) > 59.99:
            print("[WARNING] Second format is wrong")
            exit()
        else:
            s = str("%02d" % int(s))

    def update_img(self):
        os.system("wget http://www.cenapred.gob.mx/images/popoSur.JPG > /dev/null 2>&1")
        for i in range(100):
            out_img_name = "popo_sur_%03d.jpg" % (i)
            if os.path.isfile("popoSur.JPG"):
                if not os.path.isfile(out_img_name):
                    sp.call(["mv {0} {1}".format("popoSur.JPG",out_img_name)], shell=True)
                    break
        self.pixmap_volc = QPixmap(out_img_name)
        width, height = 250,170
        self.image_volc.setPixmap(self.pixmap_volc.scaled(width, height))
    


if __name__ == "__main__":
    import sys
    app = QApplication([])
    w = MainWindow()
    w.show()
    sys.exit(app.exec_())
