from obspy import read
from obspy import UTCDateTime
from numpy import absolute,log,linspace,ceil,log2,sqrt
import matplotlib.pyplot as plt
import numpy as np
from scipy import fftpack
from sys import exit
import os.path
import os
import operator

#def get_mag(ampZ,ampE,ampN):
#    amp = sqrt(ampZ**2 + ampE**2 + ampN**2)
#    mag = log(amp)+6.08
#    return mag

def get_data_here(y,mo,d,h,mi,s,stnm):
    if stnm == "PPIG":
        f = open('ssnstp.input','w')
        f.write('WIN IG PP HH_ %s/%s/%s,%s:%s:%s +10m\n' % (y,mo,d,h,mi,s))
        f.write('exit\n')
        f.close()
        os.system("SSNstp < ssnstp.input > ssnstp.out")
        os.system("rm -f ssnstp.input ssnstp.out")
        datfileE="%04d%02d%02d%02d%02d%02d.IG.PPIG.HHE.sac" % (int(y),int(mo),int(d),int(h),int(mi),int(s))
        datfileN="%04d%02d%02d%02d%02d%02d.IG.PPIG.HHN.sac" % (int(y),int(mo),int(d),int(h),int(mi),int(s))
        datfileZ="%04d%02d%02d%02d%02d%02d.IG.PPIG.HHZ.sac" % (int(y),int(mo),int(d),int(h),int(mi),int(s))
        datfile = [datfileE, datfileN, datfileZ]
    elif len(stnm) == 3:
        if stnm == "PPC":
            dat_dir="/home/popo/data_explo/" + str(y) + "/" + stnm + "/"
        elif stnm == "PPP":
            dat_dir="/home/popo/data_explo/" + str(y) + "/" + stnm + "/"
        elif stnm == "PPX":
            dat_dir="/home/popo/data_explo/" + str(y) + "/" + stnm + "/"
        datfileE = dat_dir + "%4d%02d%02d" % (float(y),float(mo),float(d)) + "_" + "%02d" % (float(h)) + "00e.sac"
        datfileN = dat_dir + "%4d%02d%02d" % (float(y),float(mo),float(d)) + "_" + "%02d" % (float(h)) + "00n.sac"
        datfileZ = dat_dir + "%4d%02d%02d" % (float(y),float(mo),float(d)) + "_" + "%02d" % (float(h)) + "00z.sac"
        datfile = [datfileE, datfileN, datfileZ]
    return datfile

def get_amp(y1):
    return abs(y1)

def instr_corr(infile,t0,t1):
    st = read(infile, starttime=t0, endtime=t1)
    stnm = st[0].stats.sac.kstnm.replace(" ","")
    comp = st[0].stats.sac.kcmpnm.replace(" ","")
    pzdir = "/home/popo/PZ/"
    pz_file = pzdir + stnm + "_" + comp + ".resp"
    t0 = st[0].stats.starttime
    pre_filt = (0.005, 0.006, 50.0, 55.0)
    seedresp = {'filename': pz_file, 'date': t0, 'units': 'VEL'}
    try:
        st.simulate(paz_remove=None, pre_filt=pre_filt, seedresp=seedresp)
    except:
        print("[*** ERROR ***] There is something wrong with instrument correction")
        print("[*** ERROR ***] Check headers and RESP file")
        print("[*** ERROR ***] RESP file: %s" % pz_file)
        print("[*** ERROR ***] SAC  file: %s" % infile)
    return st

def plot_all(filename, sta, y, mo, d, h, mi, s, f1=0.0333, f2=0.1):
    import operator
    from obspy import Trace
    plt.style.use('bmh')
    t0_str = y + "-" + mo + "-" + d +"T" + h + ":" + mi + ":" + s
    t0 = UTCDateTime(t0_str)
    tl = 5 # Time length in minutes
    trE = Trace()
    trN = Trace()
    trZ = Trace()
    traces = [trE, trN, trZ]
    for i in np.arange(3):
        traces[i] = read(filename[i], starttime=t0, endtime=t0+(tl*60))
        traces[i].detrend(type='demean')
        traces[i].detrend(type='linear')
        traces[i] = instr_corr(filename[i],t0=t0,t1=t0+(tl*60))
        traces[i].filter("bandpass", freqmin=f1, freqmax=f2)
    # PLOT
    t = np.linspace(0,tl*60, len(traces[0][0].data))
    plt.clf()
    plt.suptitle("Estación: %s\n Filtro [Hz] : %5.2f - %5.2f" % (sta,f1,f2),fontsize=18)
    plt.subplot(311)
    plt.plot(t,traces[0][0].data,label="E-W")
    plt.ylabel("Amplitud, cm/s")
    plt.legend(loc="best")
    plt.subplot(312)
    plt.plot(t,traces[1][0].data,label="N_S")
    plt.ylabel("Amplitud, cm/s")
    plt.legend(loc="best")
    plt.subplot(313)
    plt.plot(t,traces[2][0].data,label="Vertical")
    plt.ylabel("Amplitud, cm/s")
    plt.xlabel("Tiempo, s")
    plt.legend(loc="best")
    
def calc_mag(filename,sta,y,mo,d,h,mi,s,f1=0.0333,f2=0.1):
    global mag,t0
    import operator
    from obspy import Trace
    plt.style.use('bmh')
    t0_str = y + "-" + mo + "-" + d +"T" + h + ":" + mi + ":" + s
    t0 = UTCDateTime(t0_str)
    tl = 5 # Time length in minutes
    trE = Trace()
    trN = Trace()
    trZ = Trace()
    trE_o = Trace()
    trN_o = Trace()
    trZ_o = Trace()
    traces = [[trE,trE_o], [trN,trN_o], [trZ,trZ_o]]
    for i in np.arange(3):
        traces[i][0] = read(filename[i], starttime=t0, endtime=t0+(tl*60))
        traces[i][1] = read(filename[i], starttime=t0, endtime=t0+(tl*60))
        traces[i][0].detrend(type='demean')
        traces[i][0].detrend(type='demean')
        traces[i][0].detrend(type='linear')
        traces[i][1].detrend(type='linear')
        traces[i][0] = instr_corr(filename[i],t0=t0,t1=t0+(tl*60))
        traces[i][1] = instr_corr(filename[i],t0=t0,t1=t0+(tl*60))
        traces[i][0].filter("bandpass", freqmin=f1, freqmax=f2)
    # PLOT
    t = np.linspace(0,tl*60, len(traces[0][0][0].data))
    amp = []
    delta = traces[0][0][0].stats.sac.delta
        
    for i,tr in enumerate(traces):
        fig = plt.figure(figsize=(8,10))
        tr[0][0].data= tr[0][0].data*1e-2
        tr[1][0].data= tr[1][0].data*1e-2
        comp = tr[1][0].stats.channel
        ax1 = fig.add_subplot(411)
        plt.suptitle("Estación: %s\n Filtro [Hz] : %5.2f - %5.2f\nComponente: %s" % (sta,f1,f2,comp),fontsize=18)
        ax1.plot(t,tr[1][0].data,'r-',label="Datos originales")
        plt.ylabel("Amplitud, cm/s")
        plt.legend(loc="best")
        ax2 = fig.add_subplot(412)
        fft = fftpack.fft(tr[1][0].data)[:int(len(tr[1][0].data)/2)]
        fft_vec = fftpack.fftfreq(len(tr[1][0].data), delta)[:int(len(tr[1][0].data)/2)]
        plt.plot(fft_vec, abs(fft)**2,'b.',alpha=0.3)
        plt.ylabel("Amplitud")
        plt.xlabel("Frecuencia, Hz",fontsize=8)
        ax3 = fig.add_subplot(413,sharex=ax1)
        ax3.specgram(tr[1][0].data, Fs=1/tr[1][0].stats.delta, detrend="linear",cmap="plasma_r")
        plt.ylabel("Frecuencia, Hz")
        ax4 = fig.add_subplot(414,sharex=ax1)
        ax4.plot(t,tr[0][0].data,label="Datos filtrados")
        plt.legend(loc="best")
        plt.xlabel("Tiempo, s")
        plt.ylabel("Amplitud, cm/s")
        pts = plt.ginput(1, show_clicks=True)
#        pts = plt.ginput(2, show_clicks=True)
        pts = np.array(pts)
        y = pts[:,1]
#        amp.append(get_amp(y[0],y[1]))
        amp.append(get_amp(y[0]))
        plt.close()
    amp = np.sqrt(amp[0]**2 + amp[1]**2 + amp[2]**2)
    mag = np.log(amp)+6.08
    if sta == "PPC":
        mag = mag + 0.953
    elif sta == "PPP":
        mag = mag - 0.52
    elif sta == "PPX":
        mag = mag + 0.3215
    return mag, t0

def save_txt(mag,t0,f1,f2,mag_f,stdv):
    for i in range(100):
        if os.path.isfile("popo_magnitude_%03d.txt" % i):
            pass
        else:
            ofilename = "popo_magnitude_%03d.txt" % (i)
            break
    ofile = open(ofilename,"w")
    datetime = "%4d/%02d/%02d %02d:%02d:%02d" % (int(t0.year),int(t0.month),int(t0.day),
                                                       int(t0.hour),int(t0.minute),int(t0.second))
    ofile.write("STAT | MAG  |    FECHA Y HORA     | FILTRO [Hz]\n")
    ofile.write("-----------------------------------------------\n")
    for k,v in mag.items():
        ofile.write("%4s | %3.2f | %16s | %5.3f - %5.3f\n" % (str(k),float(v),datetime,f1,f2))
    ofile.write("Mk = %3.2f +- %3.2f\n" % (float(mag_f),stdv))
    ofile.close()

def pick_weight(station):
    if station == "PPIG":
        w = 1.040
    elif station == "PPC":
        w = 1.793
    elif station == "PPP":
        w = 0.434
    elif station == "PPX":
        w = 0.733
    return w

def mag_stats(magnitudes):
    dist = {}
    estac = ['PPC','PPP',' PPX','PPIG']
    pesos = []
    valores = []
    for sta,mag in magnitudes.items():
        pesos.append(pick_weight(sta))
        valores.append(mag)
    for g in range(len(valores)):
         valores[g] = valores[g] * (pesos[g] / sum(pesos))
    mk = sum(valores)
    stdv  = np.std(valores)
    return mk, stdv
