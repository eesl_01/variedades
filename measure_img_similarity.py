

from skimage.measure import structural_similarity as ssim
import matplotlib.pyplot as plt
import numpy as np
import cv2, os


def mse(imageA, imageB):
  # the 'Mean Squared Error' between the two images is the
  # sum of the squared difference between the two images;
  # NOTE: the two images must have the same dimension
  err = np.sum((imageA.astype("float") - imageB.astype("float")) ** 2)
  err /= float(imageA.shape[0] * imageA.shape[1])

  # return the MSE, the lower the error, the more "similar"
  # the two images are
  return err


def compare_images(imageA, imageB, title):
  # compute the mean squared error and structural similarity
  # index for the images
  m = mse(imageA, imageB)
  # s = ssim(imageA, imageB)
  s,diff = ssim(imageA, imageB, full=True, use_sample_covariance=True, gaussian_weights=True, sigma=1, win_size=201)
  # setup the figure
  fig = plt.figure(title)
  plt.suptitle("MSE: %.2f, SSIM: %.2f" % (m, s))

  # show first image
  ax = fig.add_subplot(1, 2, 1)
  plt.imshow(imageA, cmap=plt.cm.gray)
  plt.axis("off")

  # show the second image
  ax = fig.add_subplot(1, 2, 2)
  plt.imshow(imageB, cmap=plt.cm.gray)
  plt.axis("off")

  # show the images
  plt.show()

  return s , diff

# load the images -- the original, the original + contrast,
# and the original + photoshop
main_dir = r'D:\Users\Emilio\Dropbox\PhD\papers\enkf_synthetic\_2019\figs_2019'
original = cv2.imread(os.path.join(main_dir, '_ref_k.png'))
initial = cv2.imread(os.path.join(main_dir, '_ini_k.png'))
enkf_head = cv2.imread(os.path.join(main_dir, '_enkf_heads2.png'))
enkf_tracer = cv2.imread(os.path.join(main_dir, '_enkf_tracer2_damp01_bzw_caseg.png'))
keg_head = cv2.imread(os.path.join(main_dir, '_keg_heads2.png'))
keg_tracer = cv2.imread(os.path.join(main_dir, '_keg_tracer2.png'))


# convert the images to grayscale
original = cv2.cvtColor(original, cv2.COLOR_BGR2GRAY)[100:500,300:900]
initial = cv2.cvtColor(initial, cv2.COLOR_BGR2GRAY)[100:500,300:900]
enkf_head = cv2.cvtColor(enkf_head, cv2.COLOR_BGR2GRAY)[100:500,300:900]
enkf_tracer = cv2.cvtColor(enkf_tracer, cv2.COLOR_BGR2GRAY)[100:500,300:900]
keg_head = cv2.cvtColor(keg_head, cv2.COLOR_BGR2GRAY)[100:500,300:900]
keg_tracer = cv2.cvtColor(keg_tracer, cv2.COLOR_BGR2GRAY)[100:500,300:900]

fig = plt.figure("Images")
images = ("Original", original), ("Initial ensemble", initial), ("enkf_head", enkf_head), ("enkf_tracer", enkf_tracer), ("keg_tracer", keg_head), ("keg_tracer", keg_tracer)

# loop over the images
for (i, (name, image)) in enumerate(images):
  # show the image
  ax = fig.add_subplot(2,3, i + 1)
  ax.set_title(name)
  plt.imshow(image, cmap=plt.cm.gray)
  plt.axis("off")

# show the figure
plt.show()

# compare the images
orig, orig_img = compare_images(original, original, "Original vs. Original")
origini, origini_img = compare_images(original, initial, "Original vs. Initial Ensemble")
origenkfh, origenkfh_img = compare_images(original, enkf_head, "Original vs. enkf_head")
origenkft, origenkft_img = compare_images(original, enkf_tracer, "Original vs. enkf_tracer")
origkegh, origkegh_img = compare_images(original, keg_head, "Original vs. keg_head")
origkegt, origkegt_img = compare_images(original, keg_tracer, "Original vs. keg_tracer")

fig2 = plt.figure("Contrast")
images2 = ("Original", orig_img), ("Initial ensemble", origini_img), ("enkf_head", origenkfh_img), \
         ("enkf_tracer", origenkft_img), ("keg_head", origkegh_img), ("keg_tracer", origkegt_img)

for (i, (name, image)) in enumerate(images2):
  # show the image
  ax2 = fig2.add_subplot(2,3, i + 1)
  ax2.set_title(name)
  plt.imshow(image, cmap=plt.cm.gray)
  plt.axis("off")

#np.mean(origenkft_img[150:450, 300:890])

print('pause')
