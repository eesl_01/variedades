import numpy as np
import os
import sys
import matplotlib.pylab as plt
import pandas as pd
from scipy import interpolate
sys.path.append(os.path.abspath(os.path.join(os.curdir, '..','pycleaner','PyCleaner', 'functions')))
sys.path.append(os.path.abspath(os.path.join(os.curdir, '..','pycleaner','PyCleaner')))
import functions.fielddata as fd
from matplotlib.ticker import FormatStrFormatter

def timedelay(t):
    t = int(t)
    yfit1 = fd.interp_y(btc.signal.values, samp_times_sec.values + t, fieldsamples_conc, well, mytype='norm')
    # %% (opt 1) Attemp to make an fmin search minimizing the distance between points
    yfit2 = fd.interp_y(btc.signal.values, samp_times_sec.values + t, fieldsamples_conc, well, mytype='fmin')
    # %% (opt 2) Perform a standard linear regression to get actual concentratons for all btc
    # yfit3 = fd.interp_y(btc.signal.values, samp_times_sec.values, fieldsamples_conc, well, mytype = 'lin' )
    # %% (opt 2) Perform a standard linear regression to get actual concentratons for all btc
    yfit4 = fd.interp_y(btc.signal.values, samp_times_sec.values + t, fieldsamples_conc, well, mytype='ls_nointerc')
    plt.figure(4, figsize=(12, 6))
    plt.plot(samp_times_sec + t, fieldsamples_conc, 'bo')
    plt.plot(btc.time, yfit1, 'r--', label='fit by factor=max(conc)')
    plt.plot(btc.time, yfit2, 'b--', label='fimin search fit')
    # plt.plot(btc.time, yfit3, 'k--',label = 'regression fit')
    plt.plot(btc.time, yfit4, 'c--', label='ls no interc')
    plt.legend(loc='best')
    plt.title(well)
    plt.grid(True)
    return yfit1, yfit2, yfit4

# Directories:
mydate = '01072016'
maindir = r'D:\Users\Emilio\Field_Data_phd_lauswiesen\raw_data\tracertest'
SourceDir_Ismatec = os.path.join(maindir, 'ismatec')
btcPath = os.path.join(maindir, 'hermes', mydate, '%s_single'%mydate)
InjectionTime = '12:12:12'

sampleList = os.listdir(os.path.join(SourceDir_Ismatec, mydate))
sampleList = [x for x in sampleList if 'calib' not in x and 'config' not in x]
samp_config = pd.read_table(os.path.join(SourceDir_Ismatec, mydate, '%s_config.txt' %mydate), usecols=(0,2), skiprows=9, header=0 )

# Calibration Curve
calib_curve = pd.read_table(os.path.join(SourceDir_Ismatec, mydate, '%s_calibcurve.txt' %mydate), usecols=(0,1), skiprows=2, header=None,names=['conc','cps'] )

xnew = np.arange(calib_curve['cps'].min(), calib_curve['cps'].max(), 100)
finterp_1 = interpolate.interp1d(calib_curve['cps'], calib_curve['conc'], kind = 'linear')
finterp_2 = interpolate.interp1d(calib_curve['cps'], calib_curve['conc'], kind = 'cubic')
ffit_1 = np.poly1d(np.polyfit(calib_curve['cps'], calib_curve['conc'], 5))
ynew_interp_1 = finterp_1(xnew)
ynew_interp_2 = finterp_2(xnew)
ynew_fit_1 = ffit_1(xnew)
# Interpolate using log transformation:
# Perform interpolation (drop the last point of the calibration curve because is = 0)
calib_curve_log = calib_curve.drop([len(calib_curve)-1])
finterp_log = np.poly1d(np.polyfit(np.log(calib_curve_log['cps']), np.log(calib_curve_log['conc']), 3))
# New refined array:
xnew_log = np.arange(np.log(calib_curve_log['cps'].min()), np.log(calib_curve_log['cps'].max()), 0.025)
ynew_fit_log = finterp_log(xnew_log)
# Plot interpolated calibration curve:
fig3, ax3 = plt.subplots(1,2, figsize=(12, 4))
ax3[0].plot(calib_curve.cps, calib_curve.conc, 'b-o', label='measured')
ax3[0].plot(xnew, ynew_interp_1, 'c-o', ms=1, mew= 0.01,label='linear')
ax3[0].plot(xnew, ynew_interp_2, 'r-o', ms=1, mew= 0.01, label='cubic')
ax3[0].plot(xnew, ynew_fit_1, 'g-o', ms=1, mew= 0.01, label='polyfit')
ax3[0].legend(numpoints=1, fancybox=True, fontsize='x-small', shadow=True, loc='best')
ax3[0].grid(True)
ax3[0].set_xlabel('CPS')
ax3[0].set_ylabel('conc [$mg/l$]')
ax3[0].xaxis.set_major_formatter(FormatStrFormatter('%2.1e'))
ax3[0].tick_params(axis='x', labelsize=8)

ax3[1].plot(np.log(calib_curve.cps), np.log(calib_curve.conc), 'b-o', label='measured')
ax3[1].plot(xnew_log, ynew_fit_log, 'r-o', ms=2, mew= 0.01,label='linear')
ax3[1].grid(True)
ax3[1].legend(numpoints=1, fancybox=True, fontsize='x-small', shadow=True, loc='best')
ax3[1].set_xlabel('$ln (CPS)$')
ax3[1].set_ylabel('$ln (conc) [mg/l]$')

# Load and transform field samples into concentrations:
well = 'B6'
cur_sample_cps = pd.read_table(os.path.join(SourceDir_Ismatec, mydate, '%s.txt' %well), usecols=(0,1), skiprows=5, header=None, names=['time', 'cps'] )
blank_cps = pd.read_table(os.path.join(SourceDir_Ismatec, mydate, '%s.txt' %well), usecols=(1,), skiprows=3, header=None, names=['cps'], nrows=1)
blank_array = pd.DataFrame(data=np.ones((len(cur_sample_cps.cps) ))* blank_cps.cps[0])
cur_sample_cps_woblank = (cur_sample_cps.cps -blank_array.T).T
cur_sample_cps_woblank_norm = cur_sample_cps_woblank.apply(lambda x: (x - np.min(x)) / (np.max(x) - np.min(x)))

plt.figure(2, figsize=(12,4))
plt.title(well)
plt.plot(pd.to_datetime(cur_sample_cps.time), ffit_1(cur_sample_cps_woblank), 'rx--')
# plt.plot(pd.to_datetime(cur_sample_cps.time), finterp_1(cur_sample_cps_woblank), 'cx--')
plt.plot(pd.to_datetime(cur_sample_cps.time), np.exp(finterp_log(np.log(cur_sample_cps_woblank))), 'bx--')

# Load corresponding Breakthrough curves:
btcFile = '%s_modif.txt' %well
btc = pd.read_table(os.path.join(btcPath, btcFile), header=None, names=['time','signal'], sep=' ')
samp_times_sec = (pd.to_datetime(cur_sample_cps.time) - pd.to_datetime(InjectionTime)).dt.seconds
fieldsamples_conc = np.exp(finterp_log(np.log(cur_sample_cps_woblank)))
# fieldsamples_conc, samp_times_sec

#%%

plt.figure(3, figsize=(12,6))
pd.to_datetime(cur_sample_cps.time)
plt.title(well)
plt.plot(btc.time, btc.signal)
plt.plot(samp_times_sec, fieldsamples_conc, 'bx--')

yfit1, yfit2, yfit4 = timedelay(1000)



