import pandas as pd
import numpy as np
import matplotlib.pylab as plt#
import os
import sys
import re
import matplotlib.mlab as mlab
import collections
from scipy.fftpack import fft, ifft, fftshift, fftfreq
from scipy.interpolate import interp1d
import matplotlib.gridspec as gridspec
from scipy import signal

import matplotlib.pyplot as plt
np.random.seed(0)

# Generate or load the signal:
sampling = 1
t = np.arange(1,1001, sampling)
y = 2*np.sin(2*np.pi*t/50) + np.sin(2*np.pi*t/15) + 0.5*np.sin(2*np.pi*t/5)

n  = np.random.randn(len(t))
yn = y + n            # Add noise
yt = y + 0.005 * t    # Add linear trend

# Load the signal:
#y = np.loadtxt('test_emilio_monthTS_and_dailysignal.txt')
# t = np.arange(0,len(y),1)

# call the periodogram:
pos =  np.ceil(np.log2(t.shape[0]))
nfft = pow(2, pos)
f, Pxx = signal.periodogram(y, fs=sampling, nfft=nfft)
fn, Pxxn = signal.periodogram(yn, fs=sampling, nfft=nfft)
ft, Pxxt = signal.periodogram(yt, fs=sampling, nfft=nfft)

# Manually change to frequency domain and build periodogram, and recosntruct signal:
yfft = np.fft.fft(y)  # change to frequency domain
y_regen = ifft(yfft)
yfft_psd = np.abs(yfft) / len(yfft) / 2  # Absolute values of magnitudes

yfftn = np.fft.fft(yn)  # change to frequency domain
yn_regen = ifft(yfftn)
yfft_psdn = np.abs(yfftn) / len(yfftn) / 2  # Absolute values of magnitudes

yfft_t = np.fft.fft(yt)  # change to frequency domain
yt_regen = ifft(yfft_t)
yfft_psd_t = np.abs(yfft_t) / len(yfft_t) / 2  # Absolute values of magnitudes

num_bins = len(yfft_psd)
y2lim = int( num_bins/ 2) # find  half length of freq-series

# Create figure frame:
fig = plt.figure(1, figsize=(20,10))
gridspec.GridSpec(4,3)

# Plot time series:
ax1 = plt.subplot2grid((4,3), (0, 0), colspan=3, rowspan=1)
ax1.set_title('original time series', fontsize=15)
ax1.plot(t, y, 'k-',label='noise-free')
ax1.plot(t, yn, 'r-',label='noisy')
ax1.plot(t, yt, 'b-', label='noisy+trend')
ax1.set_ylabel('signal',  fontsize=15)
ax1.set_xlabel('time',    fontsize=15)
ax1.tick_params(axis='y', labelsize=15)
ax1.tick_params(axis='x', labelsize=15)
ax1.legend(fontsize=10, frameon=False, loc='best')

# Plot the periodogram:
ax2 = plt.subplot2grid((4,3), (1, 2), colspan=1, rowspan=1)
ax2.set_title('periodogram', fontsize=15)
ax2.plot(f, Pxx, label='noise-free')
ax2.plot(fn, Pxxn, label='noisy')
ax2.plot(f, Pxxt, label='noisy+trend')
ax2.set_ylabel('power', fontsize=15)
ax2.set_xlabel('frequency', fontsize=15)
ax2.tick_params(axis='y', labelsize=15)
ax2.tick_params(axis='x', labelsize=15)
ax2.legend(fontsize=10, frameon=False, loc='best')

# Plot the full and half power spectrum:
ax3 = plt.subplot2grid((4, 3), (1, 0), colspan=1, rowspan=1)
ax3.set_title('power spectrum', fontsize=15)
ax3.plot(np.arange(0, len(yfft_psd), 1), yfft_psd, label='noise-free')
ax3.plot(np.arange(0, len(yfft_psdn), 1), yfft_psdn, label='noise')
ax3.plot(np.arange(0, len(yfft_psd_t), 1), yfft_psd_t, label='noise+trend')
ax3.set_ylabel('power',  fontsize=15)
ax3.set_xlabel('frequency',    fontsize=15)
ax3.tick_params(axis='y', labelsize=15)
ax3.tick_params(axis='x', labelsize=15)
ax3.legend(fontsize=10, frameon=False, loc='best')

ax4 = plt.subplot2grid((4, 3), (1, 1), colspan=1, rowspan=1)
ax4.set_title('half-power spectrum', fontsize=15)
ax4.plot(np.arange(0, y2lim, 1), yfft_psd[0:y2lim], label='noise-free')
ax4.plot(np.arange(0, y2lim, 1), yfft_psdn[0:y2lim], label='noise')
ax4.plot(np.arange(0, y2lim, 1), yfft_psd_t[0:y2lim], label='noise+trend')
ax4.set_ylabel('power',  fontsize=15)
ax4.set_xlabel('frequency',    fontsize=15)
ax4.tick_params(axis='y', labelsize=15)
ax4.tick_params(axis='x', labelsize=15)
ax4.legend(fontsize=10, frameon=False, loc='best')

ax1.plot(t, np.real(y_regen),'k.',label='reg noise-free')
ax1.plot(t, np.real(yn_regen), 'r.',label='reg noisy')
ax1.plot(t, np.real(yt_regen), 'b.', label='reg noisy+trend')
ax1.legend(fontsize=10, frameon=False, loc='best')

# Design the filter
b, a = signal.butter(1, 0.1, btype='lowpass') # {‘lowpass’, ‘highpass’, ‘bandpass’, ‘bandstop’}
w, H = signal.freqz(b, a, int(np.floor(num_bins / 2.)))
y_filt = signal.lfilter(b, a, y)

ax6 = plt.subplot2grid((4,3), (2, 0), colspan=3, rowspan=1)
ax6.plot(np.arange(0, 1 + 1. / int(num_bins / 2 - 1), 1. / int(num_bins / 2 - 1)), np.abs(H), 'r-', label='filter response')
ax6.plot(np.arange(0, 1 + 1. / int(num_bins / 2 - 1), 1. / int(num_bins / 2 - 1)), yfft_psd[0:int(num_bins / 2)], 'b-', label='freq. magnitude')
ax6.set_title('filter response (normalised frequency)', fontsize=15)
ax6.set_xlabel('normalized frequency', fontsize=15)
ax6.set_ylabel('abs. magnitude', fontsize=15)
ax6.grid(True)
ax6.legend(fontsize=10, frameon=False, loc='best')
ax6.tick_params(axis='y', labelsize=15)
ax6.tick_params(axis='x', labelsize=15)


ax7 = plt.subplot2grid((4,3), (3, 0), colspan=3, rowspan=1)
ax7.set_title('filtered signal ', fontsize=15)
ax7.plot(t, y, 'k--', label='orig. signal')
ax7.plot(t, y_filt, 'k-', label='filt. signal')
ax7.set_ylabel('signal', fontsize=15)
ax7.set_tlabel('time', fontsize=15)
ax7.legend(fontsize=10, frameon=False, loc='best')
ax7.tick_params(axis='y', labelsize=15)
ax7.tick_params(axis='x', labelsize=15)

fig.tight_layout(rect=[0, 0.0, 1, 0.97])