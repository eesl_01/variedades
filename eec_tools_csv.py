import pandas as pd
import numpy as np
import matplotlib.pylab as plt#
import os
import sys
import re
import matplotlib.mlab as mlab
import collections

class Miscel(object):
    """ Parametric functions to fit to data
    Arguments:
        xData:       array with x values
        yData:       array with y values
        mytitles:    str, dataset identifier (for plotting)
        plotting:    bool, plot results or not. Default = False
    """
    def __init__(self, xData, yData, mytitles='', plotting=False):
        self.xData = xData
        self.yData = yData
        self.mytitles = mytitles
        self.plotting = plotting

    def run_plot(self, newx, newy):
        if self.plotting is True:
            myplot_obj = Mainplots(self.xData, self.yData)
            myplot_obj.xyplot(xData_new=newx, yData_new=newy, mytitle=self.mytitles)
        else:
            pass

    def remove_slope(self, idx1, idx2):

        # Line Equation slope form: y = mx + b
        y1 = np.mean(self.yData[(idx1 - 50): idx1])
        y2 = np.mean(self.yData[idx2:(idx2 + 50)])
        # Calculate slope:
        myslope = (y2 - y1 ) / (self.xData[idx2] - self.xData[idx1])
        b = y2 - (myslope * self.xData[idx2])

        ynew_temp = myslope * self.xData[idx1:idx2] + b

        ynew = self.yData[idx1:idx2] - ynew_temp

        # self.run_plot(self.xData[idx1:idx2], ynew)

        return ynew

    def areacurve(self, ydata, xdata=None, dx=1.0, func='trapz'):
        """
        Args:
            ydata:      array, input to integrate
            xdata:      list or np array, optional, if not evenly spaced series will be assumed. Default is None
            dx:         scalar, spacing between sample points when xdata is None. Default is 1
            func:       string, 'trapz' or 'simps', method to calculate the area. Default is trapz
        Returns:
            myarea:     float, area under the curve (Definite integral as approximated by trapezoidal rule)
        Examples:
        # >>> np.trapz([1,2,3])
        # >>> np.trapz([1,2,3], x=[4,6,8])
        """
        if func is 'trapz':
            myarea = np.trapz(ydata, x=xdata, dx=dx)

        elif func is 'simps':
            from scipy.integrate import simps
            myarea = simps(ydata, x=xdata, dx=dx)

        return myarea


def str_infile(myfile, mystr, index=False):
    """
    Find the string that follows a defined string. The string of interest should
    be able to be converted to "FLOAT" number(s). File format is any.
        Arguments:
        ----------
    myfile:         str, full path of the file of interest
    mystr:          str, string BEFORE the one of interest
    index:          bool, whether to retunr the line index where string was found
        Returns:
        --------
    String after the one given in the arguments. If index is True returns also
    the line indices where there was a match, as an additional array
    """
    next_str = []
    ids = []
    with open(myfile, 'r') as fp:
        counter = 0
        for idx, line in enumerate(fp):
            match = re.search(mystr + '([^,]+)', line)
            if match:
                ids.append(idx)
                next_str.append(match.group(1))
    if len(next_str) == 1:
        next_str = next_str[0]
        ids = ids[0]
    else:
        try:
            next_str = np.asarray(next_str, dtype='float')
            ids = np.asarray(ids, dtype='int')
        except:
            next_str = np.asarray(next_str, dtype='str')
            ids = np.asarray(ids, dtype='int')

    if len(next_str) > 0:
        if index is False:
            return next_str
        elif index is True:
            return ids, next_str
    else:
        print('Given string not found in file')


class Mainplots(object):

    def __init__(self, xData, yData):
        self.xData = xData
        self.yData = yData

    def pick_points(self, mytitle='', rotatemarks=False):
        """ Pick points within a XY plot, by clicking on the graph
            Arguments:
        mydata:     np array, two dim array (X,Y)
        mytitle:    str, title for the plot (name of the dataset)
            Returns:
        List with the indices of the selected points
        """

        print('Select points and close the window to continue')
        xindices = []

        def onpick(event):

            thisline = event.artist
            xdata = thisline.get_xdata()
            ydata = thisline.get_ydata()
            ind = event.ind

            xindices.append(ind[-1])
            if len(xindices) % 2 is not 0:  # if len xindices is odd
                print('1st. X = %s, Y = %s (idx:%s)' % (xdata[ind[-1]], ydata[ind[-1]], ind[-1]))

            elif len(xindices) % 2 is 0:  # if len xindices is even
                print('2nd. X = %s, Y = %s (idx:%s)' % (xdata[ind[-1]], ydata[ind[-1]], ind[-1]))

            fig.canvas.draw()

        fig = plt.figure('Pick the points')
        # Create figure:

        ax = fig.add_subplot(111)
        # Plot stuff:
        line, = ax.plot(self.xData, self.yData, 'b.', markersize=5, mew=0.1, picker=2., label='Raw Data')
        if rotatemarks is True:
            symbol = 'bx'
        else:
            symbol = 'b--'
        ax.plot(self.xData, self.yData, symbol)
        ax.set_title(mytitle)
        plt.grid(True)

        line.figure.canvas.mpl_connect('pick_event', onpick)
        plt.show()

        return xindices

    def xyplot(self, xData_new='', yData_new='', mytitle=''):

        plt.figure(figsize=(7, 5))

        if (xData_new is '') and (yData_new is ''):
            plt.plot(self.xData, self.yData, 'b-o', ms=3, label='Initial plot', linewidth=0.0)
        else:
            plt.plot(self.xData, self.yData, 'b-o', ms=3, label='Initial plot', linewidth=0.0)
            plt.plot(xData_new, yData_new, 'r-x', ms=2, label='Modified plot', linewidth=0.3)
        # plt.plot(points, 'ro', new_points,'bx')
        if mytitle != '':
            plt.title(mytitle)
        plt.xlabel('X value')
        plt.ylabel('Y value')
        plt.grid(True)
        plt.legend(loc='best')
        plt.show()
        plt.close()

#  List all files of interest:
mypath = str(input('Type in the dir of the folder containing the files: '))
# mypath = r'C:\PhD_Emilio\Emilio_PhD\_CodeDev_bitbucket\general_exercises\ARCHIVOS_EMILIO\excel data'
FileList = os.listdir(os.path.abspath(mypath))
# Loop through all the files:
for ii in FileList:
    if 'txt' in ii:
        cur_file = os.path.join(mypath, ii)
        if not os.path.isdir(cur_file[:-4]):
            os.makedirs(cur_file[:-4])

        index, dummy = str_infile(cur_file, 'Time/s,', index=True)

        # Start reading information:
        columnLabels = pd.read_table(cur_file, skiprows=index-1, nrows=1, header=None, sep=',')
        mydata = pd.read_csv(cur_file, skiprows=index+2, names=columnLabels.values[0][:])

        timespan = mydata['Time/s'][1] - mydata['Time/s'][0]
        mytime = np.arange(0, len(mydata['Time/s']), timespan)

        mydata.drop(columnLabels.values[0][0], axis=1, inplace=True)
        columnLabels.drop(0, axis=1, inplace=True)

        # Transform units to microAmperes
        mydata *= 1e6

        TransformedData = mydata.copy(deep=True)

        # Plot and activate Pick event:
        # Loop through all columns of the dataset:
        for cur_column in range(0, mydata.shape[-1]):
            flag = 'repeat'
            while flag == 'repeat':
                myPlotObj = Mainplots(mytime, mydata[columnLabels.values[0][cur_column]])
                xindices = myPlotObj.pick_points(mytitle='Select ranges\nDate: %s, Column: %s' % ( ii[:-4], columnLabels.values[0][cur_column]))

                if len(xindices) % 2 is not 0:
                    print('Select PAIR of points!!')
                if len(xindices) % 2 is 0:
                    flag = 'norepeat'

            mydata_obj = Miscel(mytime, mydata[columnLabels.values[0][cur_column]], plotting=True, mytitles='Results\nDate: %s, Column: %s' % ( ii[:-4], columnLabels.values[0][cur_column]))
            no_peaks = int(len(xindices) / 2)

            areas_dict = {}
            for cur_peak in range(0, no_peaks):
                idini =int(cur_peak * 2)
                idend = int((cur_peak * 2) + 1)
                TransformedData[columnLabels.values[0][cur_column]][xindices[idini]:xindices[idend]] = mydata_obj.remove_slope(xindices[idini], xindices[idend])

                area_trap = mydata_obj.areacurve(TransformedData[columnLabels.values[0][cur_column]][xindices[idini]:xindices[idend]], xdata=mytime[xindices[idini]:xindices[idend]], dx=1.0, func='trapz')
                area_simps = mydata_obj.areacurve(TransformedData[columnLabels.values[0][cur_column]][xindices[idini]:xindices[idend]], xdata=mytime[xindices[idini]:xindices[idend]], dx=1.0, func='simps')
                areas_dict['%s%s' % (cur_peak, columnLabels.values[0][cur_column])] = (area_trap, area_simps)
            areas_dict_ordered = collections.OrderedDict(sorted(areas_dict.items()))

            mydata_obj.run_plot(mytime, TransformedData[columnLabels.values[0][cur_column]])
            names = []
            for ss in range(0, no_peaks):
                cur_name = input('Label for the %d peak: ' % (ss + 1))
                names.append(cur_name)
            with open(os.path.join(cur_file[:-4], 'areas_%s.csv' % re.sub('/', '',columnLabels.values[0][cur_column]).strip(' ')), 'w') as f:
                f.write('id\ttrapz\tsimpson\tindices\tlabel\n')
                peak_counter = 0
                for key, value in areas_dict_ordered.items():
                    f.write('%s\t%s\t%s\ti(%s,%s)\t%s\n' % (peak_counter+1, key, value, xindices[peak_counter*2], xindices[(peak_counter*2)+1], names[peak_counter]))
                    peak_counter += 1
        TransformedData['Time'] = mytime
        TransformedData.to_csv(os.path.join(cur_file[:-4], '%s_mod.csv' % os.path.split(cur_file[:-4])[1]), sep='\t', index=False, float_format='%7.5f')
        print('Modified dataset successfully store in < %s >' % os.path.join(cur_file[:-4], '%s_mod.csv' % os.path.split(cur_file[:-4])[1]))